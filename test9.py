#服务器
import socket
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(('127.0.0.1', 6666))  #绑定
s.listen(5)  #监听
conn, address=s.accept() #阻塞
data= conn.recv(1024)   #接收
print(data.decode())
conn.sendall(("服务器已接收"+str(data.decode())).encode())
conn.close()