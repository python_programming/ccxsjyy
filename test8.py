'''
这是一个曲目榜单
'''
import pymysql
db = pymysql.connect(host='localhost',user='root',password='root',database='191108')
cursor = db.cursor()
cursor.execute('create table if not exists music(number int(5) primary key, song varchar(20), click int(10))')   #曲目排名，曲名，点击量
cursor.execute('insert into music(number,song,click) values (01,"Seasons in the sun",1000000)')
cursor.execute('insert into music(number,song,click) values (02,"Let it go",911111)')
cursor.execute('insert into music(number,song,click) values (03,"Young",800000)')
cursor.execute('select * from music')
print(cursor.fetchall())
print("\n")
cursor.execute('update music set click = 999999 where number=01')
cursor.execute('select click from music where number=01')
print(cursor.fetchmany(1))
print("\n")
cursor.execute('delete from music where number=01')
cursor.execute('select * from music')
print(cursor.fetchall())
cursor.close()
db.close()