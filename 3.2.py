'''
第一弹app中韩剧评分排列：
'''
tv=[('姜食堂',9.6),
    ('爱的迫降',9.7),
    ('我的大叔',9.4),
    ('再次出发',9.3),
    ('Traveler', 9.1),
    ('你好，再见妈妈',9.2),
    ('梨泰院class',9.8),
    ('阳光先生',8.9),
    ('咖啡朋友',9.0),
    ('新职员诞生记', 9.5),
    ]
tv=sorted(tv,key=lambda s: s[1],reverse=True)
print("第一弹app中韩剧评分榜：")
for item in tv:
    print(item)

'''
增加和减少元素：
'''
print("\n")
tv.insert(10,("德鲁纳酒店",8.0))
for item in tv:
    print(item)
print("\n")
del tv[10]
for item in tv:
    print(item)