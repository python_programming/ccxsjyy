print("王者荣耀heros：")
fuzhuhero=["孙膑","盾山","明世隐","张飞","牛魔","鬼谷子","大乔","苏烈","蔡文姬","瑶","东皇太一"]
sheshouhero=["公孙离","成吉思汗","马可波罗","孙尚香","李元芳","狄仁杰","百里守约","黄忠","伽罗","虞姬","鲁班七号"]
fashihero=["不知火舞","嫦娥","杨玉环","上官婉儿","沈梦溪","张良","司马懿","西施","周瑜"]
cikehero=["雅典娜","露娜","马超","裴擒虎","阿轲","李白","云中君","曜","兰陵王"]
tankehero=["太乙真人","张飞","牛魔","苏烈","芈月","盾山","东皇太一","庄周","廉颇","AP梦奇","白起","刘邦"]
zhanshihero=["曜","马超","狂铁","吕布","关羽","橘右京","李信","孙策","杨戬","花木兰","铠"]
print("辅助英雄")
for item in fuzhuhero:
    print(item+"\t",end='')
print("\n射手英雄")
for item in sheshouhero:
    print(item+"\t",end='')
print("\n法师英雄")
for item in fashihero:
    print(item+"\t",end='')
print("\n刺客英雄")
for item in cikehero:
    print(item+"\t",end='')
print("\n坦克英雄")
for item in tankehero:
    print(item+"\t",end='')
print("\n战士英雄")
for item in zhanshihero:
    print(item+"\t",end='')

'''
减少和增加元素：
'''
print("\n")
fuzhuhero.insert(1,"太乙真人"+"\t")    #在列表第二位增加元素
for item in fuzhuhero:
    print(item+"\t",end='')
print("\n")
del fuzhuhero[1]                      #删除添加的元素
for item in fuzhuhero:
    print(item+"\t",end='')